from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .forms import Event_Form
from .models import Event
from django.contrib import messages

response = {}

# Create your views here.
def home(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')

def experience(request):
    return render(request, 'experience.html')

def gallery(request):
    return render(request, 'gallery.html')

def contact(request):
    return render(request, 'contact.html')

def add_event(request):
    if request.method == 'POST':      
   
        # A POST request: Handle Form Upload
        form = Event_Form(request.POST) # Bind data from request.POST into a PostForm
 
        # If data is valid, proceeds to create a new post and redirect the user
        if form.is_valid():
            messages.success(request, 'Event added succesfully!')

            category = form.cleaned_data['category']
            event_name = form.cleaned_data['event_name']
            date_time = form.cleaned_data['date_time']
          
            location = form.cleaned_data['location']
            notes = form.cleaned_data['notes']
            event = Event.objects.create(category=category, 
                                        event_name=event_name, 
                                        date_time=date_time,
                                       
                                        location=location,
                                        notes=notes)
            return redirect('story4:add_event')
    else:

        form = Event_Form()

    return render(request, 'addevent.html', {
        'form': form,
    })

def all_event(request):
    allevents = Event.objects.all()
    context={
    'allevents':allevents
    }
    return render(request, 'allevent.html', context)

def delete_event(request, pk):
    Event.objects.filter(pk=pk).delete()
    allevents = Event.objects.all()
    context = {'allevents':allevents}

    return redirect('story4:all_event')
    

def delete_all(request):
    if request.method == "POST":
        Event.objects.all().delete()
        allevents = Event.objects.all()
        context = {'allevents':allevents}
        
        return redirect('story4:all_event')
    else:
        return redirect('story4:all_event')
