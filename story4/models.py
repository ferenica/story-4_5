from django.db import models
from django.utils import timezone
from datetime import datetime, date
from django.utils import timezone

# Create your models here.
class Event(models.Model):
    category = models.CharField(blank=False, max_length=20)
    event_name = models.CharField(blank = False, max_length = 30)
	#event_name = models.CharField(blank=False, max_length=30)
    date_time = models.DateTimeField(blank=False, default=timezone.now)
    location = models.TextField(blank = True, null = True)
	#location = models.TextField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True, max_length=150)

    def __str__(self):
        return self.event_name
	#def __str__(self):
		#return self.event_name

